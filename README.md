# hide-workspace-thumbnails

Hide workspace thumbnails from the overview. This frees up space for window previews, although it can make navigating between workspaces slightly more difficult.

![Screenshot of GNOME Shell with the hide-workspace-thumbnails extension enabled](screenshot.png)

## Installation

Copy `hide-workspace-thumbnails@dylanmc.ca` into `~/.local/share/gnome-shell/extensions/`, then restart GNOME Shell. Open the Extensions app, and enable this extension.

